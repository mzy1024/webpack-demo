import { createApp } from "vue";
import App from "./App.vue"
import 'element-plus/dist/index.css'
import "./assets/scss/reset.scss"
import "./routes/permission"
import router from "./routes/index"


const app = createApp(App);
app.use(router)
app.mount("#app");