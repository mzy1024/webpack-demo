import { h, render } from "vue"
import DialogTemplate from "./dialog.vue"
import { DialogPropsType, DialogType } from "./type"

class Dialog {
	container: HTMLDivElement = document.createElement("div");
	present(option:DialogType) {
		const options:DialogPropsType = {...option,closeBtn:this.disMiss.bind(this)};
		const vnode = h(DialogTemplate, options);
		render(vnode, this.container);
		document.body.insertBefore(this.container, document.body.firstChild);
	}

	disMiss() {
		setTimeout(() =>{
			render(null,this.container)
			document.body.removeChild(this.container);
		}, 150)
	}
}


export default new Dialog();