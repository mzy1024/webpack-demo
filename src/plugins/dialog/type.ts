export interface DialogType {
	title:string,
	content:string,
	confirmBtn:() => void
}


export interface DialogPropsType extends DialogType{
	closeBtn:() => void
}
