import { ref, isRef, onMounted } from 'vue'
import type { Ref } from 'vue'
import { AnyObj } from "@/types/useHandle"

interface Pagination {
    page:number,
    size:number
}
type StringArray = string[]
interface ReturnType {
    searchForm:Ref<AnyObj>,
    total:Ref<number>,
    paginationData:Ref<Pagination>,
    rowData:Ref<AnyObj | undefined>,
    createShow:Ref<boolean>,
    handleSerach():void,
    handleCurrentChange(x:number):void, 
    handleReset(...args:string[] ):void,
    handleOpenCreate(row:AnyObj | number):void
}


export default function useHandle(getList:(x:AnyObj) => void, obj?:AnyObj):ReturnType{
    const searchForm = ref<AnyObj>({})
    const paginationData = ref<Pagination>({
        page:1,
        size:20
    })
    const total = ref<number>(0);
    // 初始化参数
    if(isRef(obj)) {
        searchForm.value = <AnyObj>obj.value;
    } else {
        searchForm.value = <AnyObj>obj;
    }
    // 搜索
    function handleSerach():void{
        const obj:AnyObj = Object.assign({}, searchForm.value, paginationData.value)
        getList(obj);
    }
    // 重置
    function handleReset(...list:StringArray):void{
        for(const key in searchForm.value){
            if(!list.includes(key)){
                searchForm.value[key] = '';
            }
        }
        const obj:AnyObj = Object.assign({}, searchForm.value, paginationData.value)
        getList(obj);
    }
    // 页码变化
    function handleCurrentChange(e:number):void{
        paginationData.value.page = e;
        handleSerach()
    }
    // 新增/编辑
    const rowData = ref<AnyObj>();
    const createShow = ref(false)
    function handleOpenCreate(row:AnyObj | number):void{
        if(typeof row === 'object') rowData.value = row;
        createShow.value = true;
    }
    onMounted(() =>{
        handleSerach();
    })
    return {
        rowData,
        createShow,
        total,
        searchForm,
        paginationData,
        handleSerach,
        handleReset,
        handleOpenCreate,
        handleCurrentChange,
    }
}