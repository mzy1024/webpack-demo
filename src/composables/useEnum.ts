type EnumObj = {
    value: string | number
    name: string
}

export default function useEnum() {
    const methodEnum:EnumObj[] = [
        {
            value: 1,
            name: "支付宝",
        },
        {
            value: 2,
            name: "微信",
        },
        {
            value: 3,
            name: "云闪付",
        },
        {
            value: 4,
            name: "银行卡",
        },
        {
            value: 5,
            name: "信用卡",
        },
        {
            value: 6,
            name: "现金",
        }
    ]
    function enumFilter(value: number|string, array:EnumObj[]): EnumObj {
        let obj:EnumObj = <EnumObj>{};
        array.forEach(item =>{
            if(value === item.value){
                obj = item;
            }
        })
        return obj;
    }
    return {
        methodEnum,
        enumFilter
    }
}