import { ref, onMounted } from 'vue'
import type { Ref } from 'vue'
import { AnyObj, CategoryType } from "@/types/useHandle"


// api
import { recordPage } from "@/api/record"
import { categoryPage } from "@/api/category"

type DataType = AnyObj[];

interface OutType {
    loading:Ref<boolean>
    getList(x:AnyObj):void
    dataList:Ref<DataType>
    total:Ref<number>
    categoryList:Ref<CategoryType[]>
}
// 请求页面列表数据
export default function useList():OutType{
    const loading = ref<boolean>(false);
    const dataList = ref([]);
    const total = ref(0);
    const categoryList = ref<CategoryType[]>([]);
    function getList(params:AnyObj):void{
        const searchForm:AnyObj = {};
        for(const key in params){
            if(params[key]) searchForm[key] = params[key];
        }
        if(searchForm.createTime && searchForm.createTime.length){
            searchForm['startTime'] = searchForm.createTime[0] + " 00:00:00";
            searchForm['endTime'] = searchForm.createTime[1] + " 23:59:59";
            delete searchForm.createTime
        }
        loading.value = true;
        recordPage(searchForm).then(res =>{
            if(res.code === 200){
                dataList.value = <DataType>res.data.list;
                total.value = <number>res.data.total;
                loading.value = false;
            }
        }).catch(() =>{
            loading.value = false;
        })
    }

    function getCategoryList(){
        categoryPage({
            size:500,
            page:1
        }).then(res =>{
            if(res.code === 200){
                categoryList.value = <CategoryType[]>res.data.list;
            }
        })
    }

    onMounted(() =>{
        getCategoryList();
    })
    return {
        total,
        loading,
        dataList,
        categoryList,
        getList
    }
}