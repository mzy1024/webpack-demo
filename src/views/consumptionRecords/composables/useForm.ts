import { ref } from "vue"
import type { Ref } from "vue"
import type { FormRules, FormInstance } from "element-plus";
import { ElMessage } from "element-plus"

// api
import { recordSubmit } from "@/api/record"


interface FormData {
    money: string | number,
    method: string,
    type:number,
    category:'',
    createTime: string,
    remark: string,
}
interface OutTypes {
    rules: Ref<FormRules>,
    formInstance: Ref<FormInstance | undefined>,
    handleSubmit(): void,
    formData: Ref<FormData>
}

export default function useForm(callback: () => void): OutTypes {
    const formData = ref<FormData>({
        money: '',
        method: '',
        type:1,
        category:'',
        createTime: '',
        remark: '',
    })
    const formInstance = ref<FormInstance>();
    const rules = ref<FormRules>({
        money: [
            { required: true, message: "请输入金额", trigger: "blur" },
            { min: 0.01, message: "输入值不合法", trigger: "blur" }
        ],
        category:[{ required: true, message: "请选择支出分类", trigger: "blur" }],
        method: [{ required: true, message: "请选择支付方式", trigger: "blur" }],
        createTime: [{ required: true, message: "请选择支付时间", trigger: "blur" }]
    })

    // 提交
    function handleSubmit(): void {
        formInstance.value?.validate((result) => {
            if (result) {
                recordSubmit(formData.value).then(res =>{
                    if(res.code === 200){
                        ElMessage.success(res.msg);
                        callback();
                    }
                })
            }
        })
    }
    return {
        rules,
        formData,
        formInstance,
        handleSubmit
    }
}