import { ref } from 'vue'
import type { Ref } from 'vue'
import { AnyObj } from "@/types/useHandle"

import { categoryPage } from "@/api/category"

type OutType = {
    isLoading:Ref<boolean>
    getList(x:AnyObj):void
    total:Ref<number>
    dataList:Ref<AnyObj[]>
}
// 请求页面列表数据
export default function useList():OutType{
    const isLoading = ref<boolean>(false);
    const dataList = ref<AnyObj[]>([]);
    const total = ref<number>(0);
    function getList(params:AnyObj):void{
        const searchForm:AnyObj = {};
        for(const key in params){
            if(params[key]) searchForm[key] = params[key];
        }
        categoryPage(searchForm).then(res =>{
            dataList.value = <AnyObj[]>res.data.list;
            total.value = <number>res.data.total;
            isLoading.value = false;
        })
    }
    return {
        isLoading,
        dataList,
        total,
        getList
    }
}