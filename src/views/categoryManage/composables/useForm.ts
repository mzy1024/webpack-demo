import { ref } from "vue"
import type { Ref } from "vue"
import type { FormRules, FormInstance } from "element-plus";
import { ElMessage } from "element-plus"

// api
import { categorySubmit } from "@/api/category";
import { AnyObj } from "@/types/useHandle";

interface FormData {
    name:string
    status:number,
    type:number
}
interface OutTypes {
    rules: Ref<FormRules>,
    formInstance: Ref<FormInstance | undefined>,
    handleSubmit(): void,
    formData: Ref<FormData>
    categoryId:Ref<string>}

export default function useForm(callback: () => void): OutTypes {
    const formData = ref<FormData>({
        name:"",
        status:1,
        type:1
    })
    const formInstance = ref<FormInstance>();
    const categoryId = ref("");
    const rules = ref<FormRules>({
        name: [
            { required: true, message: "请输入分类名称", trigger: "blur" }
        ],
        type: [
            { required: true, message: "请选择分类类型", trigger: "blur" }
        ]
    })
    // 提交
    function handleSubmit(): void {
        formInstance.value?.validate((result:boolean) => {
            if (result) {
                const obj:AnyObj = {};
                if(categoryId.value) obj["id"] = categoryId.value;
                categorySubmit(Object.assign({}, formData.value, obj)).then(res =>{
                    if(res.code === 200){
                        ElMessage.success("提交成功");
                        callback();
                    } else {
                        ElMessage.error(res.msg);
                    }
                })
            }
        })
    }
    return {
        rules,
        formData,
        categoryId,
        formInstance,
        handleSubmit
    }
}