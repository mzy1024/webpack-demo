import { AnyObj } from "@/types/useHandle";
import { ref } from "vue"

type AgremntsType = AnyObj;

export default function useOption(){
    const rowData = ref<AgremntsType>({});
    const showCreate = ref<boolean>(false);
    function handleOpenCreate(row:AgremntsType):void{
        rowData.value = row;
        showCreate.value = true;
    }


    return {
        showCreate,
        rowData,
        handleOpenCreate
    }
}