import { ref } from 'vue'
import type { Ref } from 'vue'
import { AnyObj } from "@/types/useHandle"

interface OutType {
    loading:Ref<boolean>,
    getList(x:AnyObj):void
}
// 请求页面列表数据
export default function useList():OutType{
    const loading = ref<boolean>(false)
    function getList(params:AnyObj):void{
        setTimeout(() =>{
            console.log(params)
            loading.value = false;
        }, 1000)
    }
    return {
        loading,
        getList
    }
}