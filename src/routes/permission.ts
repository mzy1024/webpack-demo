import router from "./index"
import { getToken } from "@/utils/auth"
import nprogress from "nprogress"
import 'nprogress/nprogress.css'

const whiteList = ["login", "reg", "404"];

router.beforeEach((to) =>{
    nprogress.start();
    if(!getToken()){
        // 没有登录
        if(whiteList.includes(<string>to.name)){
            nprogress.done();
            return true;
        } else {
            nprogress.done;
            return {path:"/login", replace:true};
        }
    } else {
        // 已经登录
        nprogress.done();
        return true;
    }
})

router.afterEach(()=>{
    nprogress.done();

})