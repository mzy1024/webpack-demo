import { createWebHashHistory, createRouter } from "vue-router"
import Home from "@/views/home/home.vue";
import Layout from "@/layouts/index.vue";
import ConsumptionRecord from "@/views/consumptionRecords/index.vue"
import AccountManage from "@/views/accountManage/index.vue"
import CategoryManage from "@/views/categoryManage/index.vue"
// import Setting from "@/views/setting/index.vue"
import Login from "@/views/login/login.vue"
import Reg from "@/views/login/reg.vue"
import NoPage from "@/views/404/404.vue"


const routes = [
    {
        path:"/",
        redirect:"/home"
    },
    {
        path:"/",
        component:Layout,
        name:"/",
        meta:{title:"首页"},
        children:[
            {
                path:"home",
                name:"Home",
                meta:{title:"首页"},
                component:Home
            },
            {
                path:"consumption",
                name:"Consumption",
                meta:{title:"消费记录"},
                component:ConsumptionRecord
            },
            {
                path:"account",
                name:"account",
                meta:{title:"账户管理"},
                component:AccountManage
            },
            {
                path:"category",
                name:"category",
                meta:{title:"分类管理"},
                component:CategoryManage
            },
            {
                path:"setting",
                name:"setting",
                meta:{title:"设置"},
                // component:Setting,
                children:[
                    {
                        path:"userPwd",
                        name:"userPwd",
                        meta:{title:"修改密码"},
                        component:() => import(/* webpackChunkName: "views" */ "@/views/setting/userPassword/index.vue")
                    }
                ]
            }
        ]
    },
    {
        path:"/login",
        name:"login",
        component:Login
    },
    {
        path:"/reg",
        name:"reg",
        component:Reg
    },
    {
        path:"/404",
        name:"404",
        component:NoPage
    },
    {
        path:"/:path",
        redirect:"/404"
    }

]

export default createRouter({
    history:createWebHashHistory(),
    routes,
})