export type AnyObj = {
    [key:string]: number | string | boolean | AnyObj[] | AnyObj | null | undfined
}

export type ResultType = {
    code:number
    msg:string
    data:AnyObj
}

export type CategoryType = {
    name:string
    id:string
}