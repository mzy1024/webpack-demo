import request from "@/utils/http"
import { AnyObj, ResultType } from "@/types/useHandle"

// 添加
export function recordSubmit(data:AnyObj):Promise<ResultType>{
    return request({
        url:"/api/record/submit",
        method:"post",
        data
    })
}

// 登录
export function recordPage(params:AnyObj):Promise<ResultType>{
    return request({
        url:"/api/record/page",
        method:'get',
        params
    })
}