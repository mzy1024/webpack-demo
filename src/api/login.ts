import request from "@/utils/http"
import { AnyObj, ResultType } from "@/types/useHandle"

// 注册
export function reg(data:AnyObj):Promise<ResultType>{
    return request({
        url:"/reg",
        method:"post",
        data
    })
}

// 登录
export function login(data:AnyObj):Promise<ResultType>{
    return request({
        url:"/login",
        method:'post',
        data
    })
}