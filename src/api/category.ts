import request from "@/utils/http"
import { AnyObj, ResultType } from "@/types/useHandle"

// 添加
export function categorySubmit(data:AnyObj):Promise<ResultType>{
    return request({
        url:"/api/category/submit",
        method:"post",
        data
    })
}


// 添加
export function categoryPage(params:AnyObj):Promise<ResultType>{
    return request({
        url:"/api/category/page",
        method:"get",
        params
    })
}