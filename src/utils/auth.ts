
/**
 * 获取toekn
 * @returns 
 */
export function getToken():string{
    return localStorage.getItem("token") || '';
}

/**
 * 设置token
 * @param token 
 */
export function setToekn(token:string){
    localStorage.setItem("token", token);
}

