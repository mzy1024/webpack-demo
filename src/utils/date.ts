export function dateFormate(datestamp: string | number, format?: string): string {
    /**
     * 	设置默认值
     */
    format = format || "yyyy-MM-dd hh:mm:ss";
    datestamp = datestamp || new Date().valueOf();
    /**
     *  避免后端返回10位时间戳(数字),导致时间解析为1970-01-01
     */
    if (typeof datestamp === 'number') {
        if (datestamp.toString().length === 10) {
            datestamp = datestamp * 1000;
        }
    }
    /**
     *  1. 处理ios格式兼容问题
     * 	2. 处理"-"为分隔符下的日期,时间默认返回为08:00:00;
     */
    if (typeof datestamp === 'string') {
        const reg = /-/g;
        if (reg.test(datestamp)) datestamp = datestamp.replace(reg, "/");
    }
    format = format || "yyyy-MM-dd hh:mm:ss";
    const date:Date = new Date(datestamp);
    interface ObjType {
        [key:string]: any
    }
    if (date.toString() !== "Invalid Date") {
        const obj: ObjType = {
            "y+": date.getFullYear(),
            "M+": date.getMonth() + 1,
            "d+": date.getDate(),
            "h+": date.getHours(),
            "m+": date.getMinutes(),
            "s+": date.getSeconds()
        }
        // 格式化日期时间
        for (const key in obj) {
            const reg = new RegExp(key);
            if (reg.test(format)) {
                format = format.replace(reg, obj[key].toString().length === 1 ? `0${obj[key]}` : obj[key]);
            }
        }
        // 格式化星期
        const weekReg = new RegExp("w+"),
            weeks: string[] = ["日", "一", "二", "三", "四", "五", "六"];
        if (weekReg.test(format)) {
            format = format.replace(weekReg, weeks[date.getDay()]);
        }
        return format;
    } else {
        throw new Error("Invalid Date");
    }
}