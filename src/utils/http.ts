import axios from "axios";
import { getToken } from "./auth";
import { ElMessage } from "element-plus"


const base_url = process.env.base_url;

const instance = axios.create({
    baseURL: base_url,
    timeout: 10000
})
// 请求拦截
instance.interceptors.request.use((config) => {
    const token = getToken() || "";
    config.headers["authorization"] = token;
    // 请求正确
    return config
}, (error) => {
    // 请求错误
    return Promise.reject(error);
})
// 响应拦截
instance.interceptors.response.use((response) => {
    // 2xx范围内的状态码都会触发该函数
    const code = response.data.code || 200;
    const msg = response.data.msg;
    if (code === 200) {
        return response.data;
    } else if (code === 401 || code === 402) {
        ElMessage.error(msg);
        setTimeout(() => {
            localStorage.removeItem("token");
            location.href = "/#/login";
        }, 800);
        return response.data;
    } else {
        return response.data;
    }
}, (error) => {
    ElMessage.error(error.response.data.error)
    return Promise.resolve({
        code: error.response.data.status,
        msg: error.response.data.error,
        data: null,
    })
})

export default instance;