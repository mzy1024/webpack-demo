const { merge } = require("webpack-merge");
const common = require("./webpack.common");
const cssMinimizerWebpackPlugin = require("css-minimizer-webpack-plugin");
const miniCssExtractPlugin = require("mini-css-extract-plugin")

module.exports = merge(common, {
    mode:"production",
    devtool:"source-map",
    output:{
        filename:"js/[name]@[contenthash:8].js",
        publicPath:"./",
        chunkFilename:"js/[name]@chunk[contenthash:8].js",
        clean:true
    },
    optimization:{
        minimize:true,
        minimizer:[
            '...',
            new cssMinimizerWebpackPlugin()
        ],
        splitChunks:{ 
            chunks: "all"
        },
        runtimeChunk:{
            name: (entrypoint) => `runtime@${entrypoint.name}`,
        }
    },
    plugins:[
        new miniCssExtractPlugin({
            filename:"css/[name]@[contenthash:8].css"
        })      
    ]
})