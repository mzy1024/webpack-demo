const common = require("./webpack.common");
const { merge } = require("webpack-merge");
const portfinder = require("portfinder")
const friendlyErrorWebpackPlugin = require("@nuxt/friendly-errors-webpack-plugin")


const devWebpack = merge(common, {
    mode:"development",
    devtool:"cheap-module-source-map",
    output:{
        filename:'js/[name]@[hash:8].js',
    },
    devServer:{
        port:3000,
        hot:true,
        client:{
            logging:"error"
        },
        proxy: {
            "/":{
                target:"http://localhost:8080",
            }
        }
    },
    plugins: []
})


module.exports = function(){
    return new Promise((resolve) =>{
        portfinder.getPort({
            port:3000,
            stopPort:3999,
        },(err, port) =>{
            devWebpack.devServer.port = port;
            devWebpack.plugins.push(new friendlyErrorWebpackPlugin({
                compilationSuccessInfo: {
                    messages: [
                      `open http://localhost:${port}\n or http://127.0.0.1:${port}`
                    ]
                  },
            }))
            resolve(devWebpack);
        })
    })
}