const { resolve } = require("path");
const { VueLoaderPlugin } = require("vue-loader/dist/index");
const miniCssExtractPlugin = require("mini-css-extract-plugin");
const htmlWebpackPlugin = require("html-webpack-plugin")
const { DefinePlugin } = require("webpack")
const AutoImport = require('unplugin-auto-import/webpack')
const Components = require('unplugin-vue-components/webpack')
const { ElementPlusResolver } = require('unplugin-vue-components/resolvers')
const eslintWebpackPlugin = require("eslint-webpack-plugin")
const dotenvWebpack = require("dotenv-webpack")


let isProduction = process.env.NODE_ENV === "production";

function createLoader(loader) {
    return [isProduction ? miniCssExtractPlugin.loader : "vue-style-loader", "css-loader", {
        loader: "postcss-loader",
        options: {
            postcssOptions: {
                plugins: ["postcss-preset-env"]
            }
        }
    }, loader, loader === "sass-loader" ? {
        loader:"sass-resources-loader",
        options:{
            resources:[resolve(__dirname,"../src/assets/scss/_var.scss"), resolve(__dirname,"../src/assets/scss/common.scss")]
        }
    }:null].filter(Boolean);
}

module.exports = {
    stats: 'errors-only',
    entry: "./src/main.ts",
    output: {
        path: resolve(__dirname, "../dist"),
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.(t|j)s$/,
                loader: "babel-loader",
                exclude: /node_modules/,
                options: {
                    cacheDirectory: true,
                    cacheCompression: false
                }
            },
            {
                test: /\.css$/,
                use: createLoader()
            },
            {
                test: /\.scss$/,
                use: createLoader("sass-loader")
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/,
                type: "asset",
                generator: {
                    filename: "images/[name]@[hash:8][ext]",
                    publicPath: './'
                },
                parser: {
                    dataUrlCondition: {
                        maxSize: 4 * 1024,
                    }
                }
            },
            {
                test: /\.(ttf|woff2?)$/,
                type: "asset/resource",
                generator: {
                    filename: "fonts/[name]@[hash:8][ext]",
                    publicPath: "./",
                }
            }
        ]
    },
    plugins: [
        new dotenvWebpack({
            path:resolve(__dirname,`../.env.${process.env.NODE_ENV}`)
        }),
        new VueLoaderPlugin(),
        new htmlWebpackPlugin({
            template: resolve(__dirname, "../public/index.html"),
            filename: "index.html"
        }),
        new DefinePlugin({
            __VUE_OPTIONS_API__: true,
            __VUE_PROD_DEVTOOLS__: false
        }),
        AutoImport({
            dts:true,
            resolvers: [ElementPlusResolver()],
        }),
        Components({
            dts:true,
            resolvers: [ElementPlusResolver()],
        }),
        new eslintWebpackPlugin({
            extensions:["vue","js","ts"]
        }),
    ],
    resolve: {
        alias: {
            "@": resolve(__dirname, '../src'),
        },
        extensions: [".vue", ".js",".ts" , ".mjs", ".json"]
    },
    performance: false,
}